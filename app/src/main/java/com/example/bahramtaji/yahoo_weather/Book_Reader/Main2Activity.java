package com.example.bahramtaji.yahoo_weather.Book_Reader;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bahramtaji.yahoo_weather.R;
import com.example.bahramtaji.yahoo_weather.publicmethods;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main2Activity extends AppCompatActivity {
    ViewPager vp;
   // TextView  txttest;
public String txttemp;
public String txtop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        vp = findViewById(R.id.vp);
        readfile();
        publicmethods.toast(this,txttemp);
        txtop=txttemp.substring(1,50);
        Book_reader_adapter myadapter = new Book_reader_adapter(this.getSupportFragmentManager(),txttemp);
        vp.setAdapter(myadapter);
       // txttest=findViewById(R.id.txttest);

       // txttest.setText(txttemp);


        //Book_reader_adapter adapter=new Book_reader_adapter(getSupportFragmentManager());
        //vp.setAdapter(adapter);

    }
    void readfile() {
        BufferedReader reader=null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("qom.txt")));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                txttemp+=(mLine);
                txttemp+=('\n');
            }
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "Error reading file!", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }  finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }


        }
    }
}
