package com.example.bahramtaji.yahoo_weather;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.bahramtaji.yahoo_weather.modles.Forecast;
import com.example.bahramtaji.yahoo_weather.modles.Yahoo;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txtweather;
    ListView forecastslistview;
    Context context=this;
    String ipurl="http://api.ipdata.co/?api-key=19344ff6b36b81dbe0633a048669b267a089fe9aa98f1bb6bef3df2b";
    String yahoourl="https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22tehran%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bind();
        yahooweather();

    }



    private void bind(){
        txtweather=(TextView) findViewById(R.id.txtweather);
        forecastslistview=(ListView) findViewById(R.id.forecastslistview);
    }

    @Override
    public void onClick(View v) {


    }

    private void parseandshow(String responseString) {
        try {
            JSONObject jsonObject=new JSONObject(responseString);
            txtweather.setText(jsonObject.getString("country_name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
    private void yahooweather(){
        AsyncHttpClient client=new AsyncHttpClient();
        client.get(yahoourl, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                publicmethods.toast(context,"Error in load");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
parseandshowyahoo(responseString);
            }
        });

    }

    public void parseandshowyahoo(String responseString) {
        Gson gson=new Gson();
        Yahoo yahoo=gson.fromJson(responseString,Yahoo.class);
        String temp=yahoo.getQuery().getResults().getChannel().getItem().getCondition().getTemp();
        String city=yahoo.getQuery().getResults().getChannel().getLocation().getCity();
        txtweather.setText("City: "+city+" Temp: "+temp);
        List<Forecast> forecasts=
                yahoo.getQuery().getResults().getChannel().getItem().getForecast();
        forcastslistadapter adpter=new forcastslistadapter(context,forecasts);
        forecastslistview.setAdapter(adpter);
    }
}
