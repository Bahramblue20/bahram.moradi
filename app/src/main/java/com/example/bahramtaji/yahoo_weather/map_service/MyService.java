package com.example.bahramtaji.yahoo_weather.map_service;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.example.bahramtaji.yahoo_weather.publicmethods;

import java.util.logging.Handler;

public class MyService extends Service
{
   // @androidx.annotation.Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }
    android.os.Handler mHandler = new android.os.Handler();
    protected void onHandleIntent(Intent intent) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MyService.this, "service is running", Toast.LENGTH_SHORT).show();
            }
        });
    }
}