package com.example.bahramtaji.yahoo_weather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.bahramtaji.yahoo_weather.modles.Forecast;

import java.util.List;

public class forcastslistadapter extends BaseAdapter {
    Context mcontext;
    List<Forecast> forcasts;

    public forcastslistadapter(Context mcontext, List<Forecast> forcasts) {
        this.mcontext = mcontext;
        this.forcasts = forcasts;
    }

    @Override
    public int getCount() {
        return forcasts.size();
    }

    @Override
    public Object getItem(int position) {
        return forcasts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
View v=LayoutInflater.from(mcontext).inflate(R.layout.forecasts_list_item , parent , false);
       Forecast forecast=forcasts.get(position);
        ImageView imgicon=v.findViewById(R.id.imgicon);
        TextView txtday=v.findViewById(R.id.txtday);
        TextView txthigh=v.findViewById(R.id.txthigh);
        TextView txtlow=v.findViewById(R.id.txtlow);
        TextView txttext=v.findViewById(R.id.txttext);
        txtday.setText(forecast.getDay());
        txthigh.setText(forecast.getHigh());
        txtlow.setText(forecast.getLow());
        txttext.setText(forecast.getText());
        Glide.with(mcontext).load(R.drawable.cloudy_daytime).into(imgicon);
        return v;
    }
}
