package com.example.bahramtaji.yahoo_weather.maps;

import android.content.Context;
import android.content.res.Resources;
import android.icu.text.SimpleDateFormat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.bahramtaji.yahoo_weather.MainActivity;
import com.example.bahramtaji.yahoo_weather.R;
import com.example.bahramtaji.yahoo_weather.forcastslistadapter;
import com.example.bahramtaji.yahoo_weather.modles.Forecast;
import com.example.bahramtaji.yahoo_weather.modles.Yahoo;
import com.example.bahramtaji.yahoo_weather.publicmethods;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
Context context=this;
TextView txtcity;
MainActivity mi1=new MainActivity();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    bind();

    }

    private void bind() {
        txtcity=(TextView) findViewById(R.id.txtloc);
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        check_time();
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(35.56, 51.23);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
      //  mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                sydney, 15);
        mMap.animateCamera(location);
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                double lat=mMap.getCameraPosition().target.latitude;
                double lon=mMap.getCameraPosition().target.longitude;
                yahooweather(lat,lon);
            }
        });

    }
    private void yahooweather(double lat,double lon){
      String  yahoourl="https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(SELECT%20woeid%20FROM%20geo.places%20WHERE%20text%3D%22("+lat+"%2C%20"+lon+")%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        AsyncHttpClient client=new AsyncHttpClient();
        client.get(yahoourl, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                publicmethods.toast(context,"Error in load");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
              //  publicmethods.toast(context,responseString);
                parseandshowyahoo(responseString);

            }
        });

    }
    public void parseandshowyahoo(String responseString) {
        Gson gson=new Gson();
        Yahoo yahoo=gson.fromJson(responseString,Yahoo.class);
        String temp=yahoo.getQuery().getResults().getChannel().getItem().getCondition().getTemp();
        String city=yahoo.getQuery().getResults().getChannel().getLocation().getCity();
        int c= (int) Math.round((Integer.valueOf(temp)-32)/(1.8));
        txtcity.setText("City: "+city+" Temp: "+c);
      // txtcity.setText("dd");
        /* List<Forecast> forecasts=
                yahoo.getQuery().getResults().getChannel().getItem().getForecast();
        forcastslistadapter adpter=new forcastslistadapter(context,forecasts);
        forecastslistview.setAdapter(adpter);*/
    }
    private void map_theme_set() {
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.night_theme));

            if (!success) {
                Log.e("MapsActivityRaw", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MapsActivityRaw", "Can't find style.", e);
        }
    }
    private void check_time() {
        int from = 2000;
        int to = 2359;
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int t = c.get(Calendar.HOUR_OF_DAY) * 100 + c.get(Calendar.MINUTE);
        boolean isBetween = to > from && t >= from && t <= to || to < from && (t >= from || t <= to);



        int from1 = 0000;
        int to1 = 600;
        Date date1 = new Date();
        Calendar c1 = Calendar.getInstance();
        c1.setTime(date1);
        int t1 = c1.get(Calendar.HOUR_OF_DAY) * 100 + c1.get(Calendar.MINUTE);
        boolean isBetween1 = to1 > from1 && t >= from1 && t1 <= to1 || to1 < from1 && (t1 >= from1 || t1 <= to1);
 if (isBetween || isBetween1)
 {
     map_theme_set();
 }
    }
}
