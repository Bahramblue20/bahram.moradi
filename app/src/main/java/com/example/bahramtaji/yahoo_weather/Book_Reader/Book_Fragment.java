package com.example.bahramtaji.yahoo_weather.Book_Reader;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bahramtaji.yahoo_weather.R;

import org.w3c.dom.Text;

import java.security.PublicKey;

public class Book_Fragment extends Fragment {
    TextView txt;
    //static String temp;
    String txttext;
    //int numberSelected;
    public static Book_Fragment frg;


   // public static void filltext(String text){
    //   txt.setText(temp);
   //}
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //txttext="hh";
        Bundle b = getActivity().getIntent().getExtras();
        if (b!=null)
        txttext = b.getString("param");
        //numberSelected = getArguments().getInt("number");
        View v=inflater.inflate(R.layout.fragment_book,container,false);
        txt=v.findViewById(R.id.txttext);
        txt.setText(txttext);
        return v;
    }
    public static Fragment newInstance(String text) {
        Book_Fragment f = new Book_Fragment();
        f.txttext = text;
       // f.numberSelected = numberSelected;
        return f;
    }
}
